#!/bin/bash
zypper rr --all
zypper addrepo -f -p 1 http://172.25.17.38/mosos/arbat/15.4/repo/oss/ oss
zypper addrepo -f -p 1 http://172.25.17.38/mosos/arbat/15.4/repo/addon/ addon
zypper addrepo -f -p 1 http://172.25.17.38/mosos/arbat/15.4/repo/software/ software
zypper addrepo -f -p 1 http://172.25.17.38/mosos/arbat/15.4/repo/update/ update
zypper addrepo -f -p 99 http://172.25.17.38/opensuse/leap/15.4/repo/oss/ oss-suse
